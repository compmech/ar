The files `H45_deviation.glb`, `H45_deviation.usdz`, `HA_deviation.glb`, `HA_deviation.usdz`, `HB_deviation.glb`, `HB_deviation.usdz`, `HC_deviation.glb`, `HC_deviation.usdz`, `HCH_deviation.glb`, `HCH_deviation.usdz` in this directory contains postprocessed data based on:

Jiang, L., Pyrak-Nolte, L., Yoon, H., Bobet, A. and Morris, J. (2021): Calibration data set for damage mechanics challenge on brittle-ductile material, [doi.org/10.4231/QF39-Q924](https://doi.org/10.4231/QF39-Q924)

Jiang, L., Pyrak-Nolte, L., Yoon, H., Bobet, A. and Morris, J. (2021): Digital image, x-ray ct , xrd and ultrasonic data sets for damage mechanics challenge on brittle-ductile material, [doi.org/10.4231/2E8M-W085](https://doi.org/10.4231/2E8M-W085)
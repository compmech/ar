# Cahn Hilliard phase separation

Below, a phase-separation process is shown, as computed by solving the Cahn-Hilliard equations.
To highlight the evolution, the domain is deformed based on the concentration $c$ (also indicated by the color map).

<model-viewer src="_static/models/cahn_hilliard/cahn_hilliard.glb" autoplay ar="" ar-scale="auto" camera-controls="" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/cahn_hilliard/cahn_hilliard.usdc" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(c\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

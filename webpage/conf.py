# project information
project = "CMG AR Suite"
copyright = "2024, Computational Mechanics Group, ETH Zurich"
author = "Computational Mechanics Group, ETH Zurich"

# extensions for Sphinx
extensions = ["myst_parser"]  # "sphinx_new_tab_link" creates recursion error
myst_enable_extensions = ["amsmath", "dollarmath"]

# html configuration
html_context = {"default_mode": "dark"}
html_static_path = ["_static"]
html_theme = "pydata_sphinx_theme"
html_favicon = "_static/favicon.ico"
html_theme_options = {
    "logo": {
        "image_light": "cmg_logo_inline_pos.png",
        "image_dark": "cmg_logo_inline_neg.png",
    },
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.ethz.ch/compmech/ar",
            "icon": "fa-brands fa-gitlab",
        },
        {
            "name": "Computational Mechanics Group",
            "url": "https://compmech.ethz.ch/",
            "icon": "fa-solid fa-graduation-cap",
        },
        {
            "name": "Twitter",
            "url": "https://twitter.com/LDeLorenzisETHZ",
            "icon": "fa-brands fa-twitter",
        },
    ],
    "secondary_sidebar_items": [],
    "header_links_before_dropdown": 1,
    "header_dropdown_text": "Topics",
    "navbar_persistent": "search-button",
    "search_bar_text": "search for AR models...",
    "article_header_start": [],
    "article_header_end": [],
    # "article_footer_items": ["breadcrumbs"],
    "show_prev_next": False,
    "footer_start": ["copyright"],
    "footer_center": ["sphinx-version"],
    "footer_end": ["theme-version"],
}
html_sidebars = {"**": []}
html_show_sourcelink = False
html_css_files = ["css/custom.css"]
html_title = "CMG AR Suite"
html_favicon = "_static/favicon.ico"
html_js_files = [("scripts/model-viewer.min.js", {"type": "module"})]

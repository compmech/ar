# Damage Mechanics Challenge

In 2022, we took part in the [Damage Mechanics Challenge](https://purr.purdue.edu/groups/damagemechanicschallenge), where the assignment was to most accurately predict a complex, mixed-mode fracture in an anisotropic, 3D-printed rock material.
To this end, experimental data from various tests were provided [1,2], based on which a computational approach was to be calibrated.
We chose the well-established phase-field approach for brittle fracture and focused on developing a calibration procedure to properly set the material parameters.
The main idea was to recast the parameter calibration into an optimization problem, with the objective of minimizing the deviation between the numerical and experimental force-displacement response.
Our detailed approach and findings can be found in the paper [3].
Below we provide AR models to visualize the central 3D aspect of the obtained crack surfaces for various three point bending tests.
These include the HC (centered notch), HB (off-centered notch), HA (far off-centered notch) and H45 (angled initial notch) test, which we used to calibrate the parameters of our model, as well as the DMC test, which is the actual challenge prediction.
All the experimental data shown is postprocessed data taken from [1,2].

Note that only the elements with $d \geq 0.25$ are shown in the outline of the respective three point bending test geometry.

## DMC Test
<model-viewer src="_static/models/dmc/HCH_phasefield.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(d\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

<model-viewer src="_static/models/dmc/HCH_deviation.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_deviation.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1.90\)mm</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\Delta s\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## HC Test
<model-viewer src="_static/models/dmc/HC_phasefield.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HC_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(d\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

<model-viewer src="_static/models/dmc/HC_deviation.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HC_deviation.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1.11\)mm</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\Delta s\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## HB Test
<model-viewer src="_static/models/dmc/HB_phasefield.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HB_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(d\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

<model-viewer src="_static/models/dmc/HB_deviation.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HB_deviation.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1.80\)mm</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\Delta s\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## HA Test
<model-viewer src="_static/models/dmc/HA_phasefield.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HA_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(d\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


<model-viewer src="_static/models/dmc/HA_deviation.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HA_deviation.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(4.68\)mm</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\Delta s\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## H45 Test
<model-viewer src="_static/models/dmc/H45_phasefield.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/H45_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(d\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

<model-viewer src="_static/models/dmc/H45_deviation.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/H45_deviation.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1.18\)mm</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\Delta s\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Material Properties
To visualize the material constants we calibrated for the assumption of orthotropic material, we employ the strategy and implementation presented in [4].
Below, we show the Young's modulus $E$ as well as the average and maximum of the Poisson's ratio $\nu$ along a director $\boldsymbol{d}$.
<model-viewer src="_static/models/dmc/E.glb" ar="" ar-scale="auto" camera-controls touch-action="pan-y" shadow-intensity="1" alt="Youngs modulus" ios-src="_static/models/dmc/E.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(2220\) MPa</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(E\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(280\) MPa</span></div>
</div>
<model-viewer src="_static/models/dmc/nu_avg.glb" ar="" ar-scale="auto" camera-controls touch-action="pan-y" shadow-intensity="1" alt="Poissons ratio mean" ios-src="_static/models/dmc/nu_avg.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(0.53\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\text{mean}(\nu)\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0.24\)</span></div>
</div>
<model-viewer src="_static/models/dmc/nu_max.glb" ar="" ar-scale="auto" camera-controls touch-action="pan-y" shadow-intensity="1" alt="Poissons ratio max" ios-src="_static/models/dmc/nu_max.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_turbo.png" alt="colormap_turbo" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1.46\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\max(\nu)\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0.33\)</span></div>
</div>


## References
<table>
    <tr style="vertical-align:top">
        <td>[1]</td>
        <td>Jiang, L., Pyrak-Nolte, L., Yoon, H., Bobet, A. and Morris, J. (2021): Calibration data set for damage mechanics challengeon brittle-ductile material, <a href="https://doi.org/10.4231/QF39-Q924">doi.org/10.4231/QF39-Q924</a></td>
    </tr>
    <tr style="vertical-align:top">
        <td>[2]</td>
        <td>Jiang, L., Pyrak-Nolte, L., Yoon, H., Bobet, A. and Morris, J. (2021): Digital image, x-ray ct , xrd and ultrasonic data sets for damage mechanics challenge on brittle-ductile material, <a href="https://doi.org/10.4231/2E8M-W085">doi.org/10.4231/2E8M-W085</a></td>
    </tr>
    <tr style="vertical-align:top">
        <td>[3]</td>
        <td>J. Heinzmann, P. Carrara, C. Luo, M. Manav, A. Mishra, S. Nagaraja, H. Oudich, F. Vicentini, D. De Lorenzis (2024): Calibration and validation of a phase-field model of brittle fracture within the damage mechanics challenge, Engineering Fracture Mechanics 307 (110319), <a href="https://doi.org/10.1016/j.engfracmech.2024.110319">doi.org/10.1016/j.engfracmech.2024.110319</a></td>
    </tr>
    <tr style="vertical-align:top">
        <td>[4]</td>
        <td>J. Nordmann, M. Aßmus and H. Altenbach (2018): Visualising elastic anisotropy: theoretical background and computational implementation, Continuum Mechanics and Thermodynamics 30: 689–708, <a href="https://doi.org/10.1007/s00161-018-0635-9">doi.org/10.1007/s00161-018-0635-9</a></td>
    </tr>
</table>

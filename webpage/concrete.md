# Phase-field modelling of fracture in concrete
The following visualizations show the phase-field variable $\alpha$ for different tests.

## Test 1 with Vol/Dev split at scan 7 
<model-viewer src="_static/models/concrete/spec4_amor_pos7.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 1 with Vol/Dev split at scan 7 
<model-viewer src="_static/models/concrete/spec4_amor_pos7_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Test 1 with Spectral split at scan 7 
<model-viewer src="_static/models/concrete/spec4_miehe_pos7.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 1 with Spectral split at scan 7 
<model-viewer src="_static/models/concrete/spec4_miehe_pos7_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Test 1 with Vol/Dev split at scan 10
<model-viewer src="_static/models/concrete/spec4_amor_pos10.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 1 with Vol/Dev split at scan 10
<model-viewer src="_static/models/concrete/spec4_amor_pos10_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Test 1 with Spectral split at scan 10
<model-viewer src="_static/models/concrete/spec4_miehe_pos10.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 1 with Spectral split at scan 10 
<model-viewer src="_static/models/concrete/spec4_miehe_pos10_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Test 2 with Vol/Dev split at scan 10 
<model-viewer src="_static/models/concrete/spec3_amor_pos10.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 2 with Vol/Dev split at scan 10 
<model-viewer src="_static/models/concrete/spec3_amor_pos10_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Test 2 with Spectral split at scan 10 
<model-viewer src="_static/models/concrete/spec3_miehe_pos10.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 2 with Spectral split at scan 10 
<model-viewer src="_static/models/concrete/spec3_miehe_pos10_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Test 3 with Vol/Dev split at scan 8 
<model-viewer src="_static/models/concrete/spec5_pos7_amor.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 3 with Vol/Dev split at scan 8 
<model-viewer src="_static/models/concrete/spec5_pos7_amor_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Test 3 with Spectral split at scan 8 
<model-viewer src="_static/models/concrete/spec5_pos7_miehe.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 3 with Spectral split at scan 8
<model-viewer src="_static/models/concrete/spec5_pos7_miehe_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Test 3 with Vol/Dev split at scan 9
<model-viewer src="_static/models/concrete/spec5_pos8_amor.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 3 with Vol/Dev split at scan 9
<model-viewer src="_static/models/concrete/spec5_pos8_amor_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>


## Test 3 with Spectral split at scan 9
<model-viewer src="_static/models/concrete/spec5_pos8_miehe.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>

## Crack pattern for test 3 with Spectral split at scan 9
<model-viewer src="_static/models/concrete/spec5_pos8_miehe_expnum.glb" ar="" ar-scale="auto" camera-controls camera-orbit="-20deg 75deg 1m" touch-action="pan-y" shadow-intensity="1" alt="3D model for a crack" ios-src="_static/models/dmc/HCH_phasefield.usdz" xr-environment="" ar-status="not-presenting"></model-viewer>
<div class="container">
    <img src="_static/colormap_coolwarm.png" alt="colormap_coolwarm" width="40%" height="auto">
    <div class="colormap_max"><span class="math notranslate nohighlight">\(1\)</span></div>
    <div class="colormap_mid"><span class="math notranslate nohighlight">\(\alpha\)</span></div>
    <div class="colormap_min"><span class="math notranslate nohighlight">\(0\)</span></div>
</div>
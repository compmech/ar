# Augmented Reality for Scientific Visualization by the Computational Mechanics Group
This is the Gitlab repo to create and manage the AR models on display at [ar.compmech.ethz.ch](https://ar.compmech.ethz.ch).

For questions, please contact either J. Heinzmann ([jheinzmann@ethz.ch](mailto:jheinzmann@ethz.ch)) or H. Oudich ([houdich@ethz.ch](mailto:houdich@ethz.ch)).

# Contents of this repository
This repository contains
- [presentation slides](https://gitlab.ethz.ch/compmech/ar/-/blob/main/Scientific_Visualization_using_AR_for_Phase_Field_Results_GAMMPF_2024.pdf) from our talk at the GAMM Phase-Field Workshop in Februrary 2024,
- the `*.glb` and `*.usdz` models for several different examples in the `models/` folder,
- examples for the [ParaView](https://www.paraview.org/) and [Blender](https://www.blender.org/) Python scripts to generate the former in the `scripts/` folder,
- as well as all files necessary for the automatic website generation with [Sphinx](https://www.sphinx-doc.org/en/master/). The html-commands to actually embed the models on a website can be seen in any one of the Markdown-files for the individual content pages. The CI-pipeline described in part by `.gitlab-ci.yml` automatically generates the website and uploads it to the webserver.

# Software versions
One may encounter postprocessing issues related to software, package or OS versions.
As of now, the preferred and tested setup is:
- System OS: `Windows` or `macOS`
- Blender version [2.83LTS](https://www.blender.org/download/lts/2-83/)
- USDZ package extension version [0.5](https://github.com/robmcrosby/BlenderUSDZ?tab=readme-ov-file)
- Paraview version [5.xx](https://www.paraview.org/download/)


With newer Blender versions, graphical issues were observed in exporting animated `*.glb` and `*.usdz` objects with our animation approach detailed in the tutorial. As for `Linux OS` combined with `Blender 2.83 LTS`, export error messages were encountered. Feel free to update us on any other encountered problems or improvements made with the current or alternative setups.

# Citation
If you want to reference this repository in your research, please cite the following article: [![DOI](https://img.shields.io/badge/DOI-10.1016%2Fj.engfracmech.2024.110319-blue)](https://doi.org/10.1016/j.engfracmech.2024.110319)

# License
This repository is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
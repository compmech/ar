# ------------------------------------- READ ME ------------------------------------------------------
# >>>>>>>>>>>>>>>
# The following python script is used in Paraview python terminal and serves as an initiation template
# The script below brings out the Damage Mechanics Challenge objects in the frame to extract their surfaces
# and the generate a .ply file to be later postprocessed in Blender
# One may use the option : Paraview/Tools/Start Trace to create a macro 'python script'
# >>>>>>>>>>>>>>>
# To check on the final results, please visit https://ar.compmech.ethz.ch
# >>>>>>>>>>>>>>>
# -----------------------------------------------------------------------------------------------------

# dependencies
from paraview.simple import *

# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


# variables
path_inputfile  = input("enter the absolute path of the result file")
path_outputfile = input("enter the absolute path for storing the ply file")
threshold_phasefield_min = float(input("enter the threshold for the phase-field"))

print("input file: ", path_inputfile)
print("chosen phase-field threshold:", threshold_phasefield_min)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")

# create a new 'XML Unstructured Grid Reader'
hxx_fieldsvtu = XMLUnstructuredGridReader(registrationName=path_inputfile, FileName=[path_inputfile])
hxx_fieldsvtu.CellArrayStatus = []
hxx_fieldsvtu.PointArrayStatus = ["u", "d"]
hxx_fieldsvtu.TimeArray = "TimeValue"

# Properties modified on
hxx_fieldsvtu.TimeArray = "None"
UpdatePipeline(time=0.0, proxy=hxx_fieldsvtu)

# create a new 'Threshold'
threshold1 = Threshold(registrationName="Threshold1", Input=hxx_fieldsvtu)
threshold1.Scalars = ["POINTS", "d"]
threshold1.LowerThreshold = threshold_phasefield_min
threshold1.UpperThreshold = 2
threshold1.ThresholdMethod = "Between"
threshold1.AllScalars = 1
threshold1.UseContinuousCellRange = 0
threshold1.Invert = 0
UpdatePipeline(time=0.0, proxy=threshold1)

# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName="CleantoGrid1", Input=threshold1)
cleantoGrid1.Tolerance = 0.0
cleantoGrid1.AbsoluteTolerance = 1.0
cleantoGrid1.ToleranceIsAbsolute = 0
UpdatePipeline(time=0.0, proxy=cleantoGrid1)

# create a new 'Extract Surface'
extractSurface1 = ExtractSurface(registrationName="ExtractSurface1", Input=cleantoGrid1)
extractSurface1.PieceInvariant = 1
extractSurface1.NonlinearSubdivisionLevel = 1
extractSurface1.FastMode = 0
try:
    # on some paraview versions, this does not work
    extractSurface1.RemoveGhostInterfaces = 1
except:
    pass
UpdatePipeline(time=0.0, proxy=extractSurface1)

# create a new 'Generate Surface Normals'
generateSurfaceNormals1 = GenerateSurfaceNormals(registrationName="GenerateSurfaceNormals1", Input=extractSurface1)
generateSurfaceNormals1.FeatureAngle = 30.0
generateSurfaceNormals1.Splitting = 1
generateSurfaceNormals1.Consistency = 1
generateSurfaceNormals1.FlipNormals = 0
generateSurfaceNormals1.NonManifoldTraversal = 1
generateSurfaceNormals1.ComputeCellNormals = 0
generateSurfaceNormals1.PieceInvariant = 1
UpdatePipeline(time=0.0, proxy=generateSurfaceNormals1)

# get color transfer function/color map for 'd'
generateSurfaceNormals1Display = Show(generateSurfaceNormals1, renderView1, "GeometryRepresentation")
ColorBy(generateSurfaceNormals1Display, ("POINTS", "d"))
dLUT = GetColorTransferFunction("d")

# Save the ply file
SaveData(
    path_outputfile,
    proxy=generateSurfaceNormals1,
    ChooseArraysToWrite=0,
    PointDataArrays=["Normals", "d"],
    CellDataArrays=[],
    FieldDataArrays=[],
    VertexDataArrays=[],
    EdgeDataArrays=[],
    RowDataArrays=[],
    WriteTimeSteps=0,
    Filenamesuffix="_%d",
    NumberOfIORanks=1,
    RankAssignmentMode="Contiguous",
    FileType="Binary",
    EnableColoring=1,
    EnableAlpha=0,
    ColorArrayName=["POINTS", "d"],
    LookupTable=dLUT,
)

# print success message
print("successfully output the file ", path_outputfile, "!")

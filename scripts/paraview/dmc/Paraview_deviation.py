# ------------------------------------- READ ME ------------------------------------------------------
# >>>>>>>>>>>>>>>
# The following python script is used in Paraview python terminal and serves as an initiation template
# The script below brings out the Damage Mechanics Challenge objects in the frame to extract their surfaces
# and the generate a .ply file to be later postprocessed in Blender
# One may use the option : Paraview/Tools/Start Trace to create a macro 'python script'
# >>>>>>>>>>>>>>>
# To check on the final results, please visit https://ar.compmech.ethz.ch
# >>>>>>>>>>>>>>>
# -----------------------------------------------------------------------------------------------------

# ****************************************************************************************************************************
# ====== We import the experimental crack, the deviation with respect to numerics, and the geometry outline ==========
# ****************************************************************************************************************************

# dependencies
from paraview.simple import *

# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


# variables
path_inputfile_num  = input("enter the absolute path of the numerical crack surface file")
path_outputfile_num = input("enter the absolute path for storing the numerical ply file")
path_inputfile_exp  = input("enter the absolute path of the experimental crack surface file")
path_outputfile_exp = input("enter the absolute path for storing the experimental ply file")

print("input file numerical: ", path_inputfile_num)
print("input file experimental: ", path_inputfile_exp)

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")


# =============================================================================================================
# 1) N U ME R I C A L   C R A C K   S U R F A C E
# =============================================================================================================
# Open the numerical crack surface (deviation) file : create a new 'XML Unstructured Grid Reader'
file_deviation_vtk = LegacyVTKReader(registrationName=path_inputfile_num, FileNames=[path_inputfile_num])

# show data in view (Colors of the deviation)
file_deviationvtkDisplay = Show(file_deviation_vtk, renderView1, "StructuredGridRepresentation")

# get opacity transfer function/opacity map for 'deviation'
deviationPWF = GetOpacityTransferFunction("deviation")

# get color transfer function/color map for 'deviation'
deviationLUT = GetColorTransferFunction("deviation")

# get 2D transfer function for 'deviation'
try:
    deviationTF2D = GetTransferFunction2D("deviation")
except:
    # for older functions
    pass

# fmt: off
deviationLUT.RGBPoints = [0.0, 0.18995, 0.07176, 0.23217, 0.39216, 0.19483, 0.08339, 0.26149, 0.7843100000000001, 0.19956, 0.09498, 0.29024, 1.1764999999999999, 0.20415, 0.10652, 0.31844, 1.5685999999999998, 0.2086, 0.11802, 0.34607, 1.9608, 0.21291, 0.12947, 0.37314, 2.3529, 0.21708, 0.14087, 0.39964, 2.7451, 0.22111, 0.15223, 0.42558, 3.1372999999999998, 0.225, 0.16354, 0.45096, 3.5294, 0.22875, 0.17481, 0.47578, 3.9216, 0.23236, 0.18603, 0.50004, 4.3137, 0.23582, 0.1972, 0.52373, 4.7059, 0.23915, 0.20833, 0.54686, 5.098, 0.24234, 0.21941, 0.56942, 5.4902, 0.24539, 0.23044, 0.59142, 5.8824000000000005, 0.2483, 0.24143, 0.61286, 6.2745, 0.25107, 0.25237, 0.63374, 6.6667000000000005, 0.25369, 0.26327, 0.65406, 7.0588, 0.25618, 0.27412, 0.67381, 7.4510000000000005, 0.25853, 0.28492, 0.693, 7.8431, 0.26074, 0.29568, 0.71162, 8.235299999999999, 0.2628, 0.30639, 0.72968, 8.627500000000001, 0.26473, 0.31706, 0.74718, 9.0196, 0.26652, 0.32768, 0.76412, 9.4118, 0.26816, 0.33825, 0.7805, 9.8039, 0.26967, 0.34878, 0.79631, 10.196, 0.27103, 0.35926, 0.81156, 10.588000000000001, 0.27226, 0.3697, 0.82624, 10.979999999999999, 0.27334, 0.38008, 0.84037, 11.373, 0.27429, 0.39043, 0.85393, 11.765, 0.27509, 0.40072, 0.86692, 12.157, 0.27576, 0.41097, 0.87936, 12.549, 0.27628, 0.42118, 0.89123, 12.940999999999999, 0.27667, 0.43134, 0.90254, 13.333, 0.27691, 0.44145, 0.91328, 13.725000000000001, 0.27701, 0.45152, 0.92347, 14.118, 0.27698, 0.46153, 0.93309, 14.510000000000002, 0.2768, 0.47151, 0.94214, 14.902000000000001, 0.27648, 0.48144, 0.95064, 15.293999999999999, 0.27603, 0.49132, 0.95857, 15.686, 0.27543, 0.50115, 0.96594, 16.078, 0.27469, 0.51094, 0.97275, 16.470999999999997, 0.27381, 0.52069, 0.97899, 16.863, 0.27273, 0.5304, 0.98461, 17.255000000000003, 0.27106, 0.54015, 0.9893, 17.647, 0.26878, 0.54995, 0.99303, 18.038999999999998, 0.26592, 0.55979, 0.99583, 18.431, 0.26252, 0.56967, 0.99773, 18.823999999999998, 0.25862, 0.57958, 0.99876, 19.216, 0.25425, 0.5895, 0.99896, 19.608, 0.24946, 0.59943, 0.99835, 20.0, 0.24427, 0.60937, 0.99697, 20.392, 0.23874, 0.61931, 0.99485, 20.784, 0.23288, 0.62923, 0.99202, 21.176000000000002, 0.22676, 0.63913, 0.98851, 21.569, 0.22039, 0.64901, 0.98436, 21.961, 0.21382, 0.65886, 0.97959, 22.353, 0.20708, 0.66866, 0.97423, 22.745, 0.20021, 0.67842, 0.96833, 23.137, 0.19326, 0.68812, 0.9619, 23.529, 0.18625, 0.69775, 0.95498, 23.921999999999997, 0.17923, 0.70732, 0.94761, 24.314, 0.17223, 0.7168, 0.93981, 24.706, 0.16529, 0.7262, 0.93161, 25.098, 0.15844, 0.73551, 0.92305, 25.490000000000002, 0.15173, 0.74472, 0.91416, 25.881999999999998, 0.14519, 0.75381, 0.90496, 26.275, 0.13886, 0.76279, 0.8955, 26.667, 0.13278, 0.77165, 0.8858, 27.059, 0.12698, 0.78037, 0.8759, 27.450999999999997, 0.12151, 0.78896, 0.86581, 27.843, 0.11639, 0.7974, 0.85559, 28.235, 0.11167, 0.80569, 0.84525, 28.62700000000001, 0.10738, 0.81381, 0.83484, 29.020000000000003, 0.10357, 0.82177, 0.82437, 29.412, 0.10026, 0.82955, 0.81389, 29.804000000000002, 0.0975, 0.83714, 0.80342, 30.196, 0.09532, 0.84455, 0.79299, 30.587999999999997, 0.09377, 0.85175, 0.78264, 30.98, 0.09287, 0.85875, 0.7724, 31.373, 0.09267, 0.86554, 0.7623, 31.765, 0.0932, 0.87211, 0.75237, 32.157000000000004, 0.09451, 0.87844, 0.74265, 32.549, 0.09662, 0.88454, 0.73316, 32.940999999999995, 0.09958, 0.8904, 0.72393, 33.333, 0.10342, 0.896, 0.715, 33.72500000000001, 0.10815, 0.90142, 0.70599, 34.117999999999995, 0.11374, 0.90673, 0.69651, 34.510000000000005, 0.12014, 0.91193, 0.6866, 34.902, 0.12733, 0.91701, 0.67627, 35.294, 0.13526, 0.92197, 0.66556, 35.686, 0.14391, 0.9268, 0.65448, 36.077999999999996, 0.15323, 0.93151, 0.64308, 36.471, 0.16319, 0.93609, 0.63137, 36.863, 0.17377, 0.94053, 0.61938, 37.25500000000001, 0.18491, 0.94484, 0.60713, 37.647000000000006, 0.19659, 0.94901, 0.59466, 38.03900000000001, 0.20877, 0.95304, 0.58199, 38.431, 0.22142, 0.95692, 0.56914, 38.824, 0.23449, 0.96065, 0.55614, 39.216, 0.24797, 0.96423, 0.54303, 39.608, 0.2618, 0.96765, 0.52981, 40.0, 0.27597, 0.97092, 0.51653, 40.392, 0.29042, 0.97403, 0.50321, 40.784, 0.30513, 0.97697, 0.48987, 41.176, 0.32006, 0.97974, 0.47654, 41.569, 0.33517, 0.98234, 0.46325, 41.961, 0.35043, 0.98477, 0.45002, 42.35300000000001, 0.36581, 0.98702, 0.43688, 42.745, 0.38127, 0.98909, 0.42386, 43.137, 0.39678, 0.99098, 0.41098, 43.529, 0.41229, 0.99268, 0.39826, 43.922, 0.42778, 0.99419, 0.38575, 44.314, 0.44321, 0.99551, 0.37345, 44.706, 0.45854, 0.99663, 0.3614, 45.098, 0.47375, 0.99755, 0.34963, 45.49, 0.48879, 0.99828, 0.33816, 45.882, 0.50362, 0.99879, 0.32701, 46.275, 0.51822, 0.9991, 0.31622, 46.666999999999994, 0.53255, 0.99919, 0.30581, 47.059, 0.54658, 0.99907, 0.29581, 47.45100000000001, 0.56026, 0.99873, 0.28623, 47.843, 0.57357, 0.99817, 0.27712, 48.23499999999999, 0.58646, 0.99739, 0.26849, 48.626999999999995, 0.59891, 0.99638, 0.26038, 49.02, 0.61088, 0.99514, 0.2528, 49.412, 0.62233, 0.99366, 0.24579, 49.803999999999995, 0.63323, 0.99195, 0.23937, 50.196, 0.64362, 0.98999, 0.23356, 50.588, 0.65394, 0.98775, 0.22835, 50.980000000000004, 0.66428, 0.98524, 0.2237, 51.373000000000005, 0.67462, 0.98246, 0.2196, 51.76500000000001, 0.68494, 0.97941, 0.21602, 52.157, 0.69525, 0.9761, 0.21294, 52.549, 0.70553, 0.97255, 0.21032, 52.941, 0.71577, 0.96875, 0.20815, 53.333, 0.72596, 0.9647, 0.2064, 53.725, 0.7361, 0.96043, 0.20504, 54.118, 0.74617, 0.95593, 0.20406, 54.510000000000005, 0.75617, 0.95121, 0.20343, 54.901999999999994, 0.76608, 0.94627, 0.20311, 55.294, 0.77591, 0.94113, 0.2031, 55.686, 0.78563, 0.93579, 0.20336, 56.077999999999996, 0.79524, 0.93025, 0.20386, 56.471, 0.80473, 0.92452, 0.20459, 56.863, 0.8141, 0.91861, 0.20552, 57.255, 0.82333, 0.91253, 0.20663, 57.647000000000006, 0.83241, 0.90627, 0.20788, 58.03899999999999, 0.84133, 0.89986, 0.20926, 58.431, 0.8501, 0.89328, 0.21074, 58.824, 0.85868, 0.88655, 0.2123, 59.216, 0.86709, 0.87968, 0.21391, 59.608000000000004, 0.8753, 0.87267, 0.21555, 60.0, 0.88331, 0.86553, 0.21719, 60.392, 0.89112, 0.85826, 0.2188, 60.784000000000006, 0.8987, 0.85087, 0.22038, 61.175999999999995, 0.90605, 0.84337, 0.22188, 61.568999999999996, 0.91317, 0.83576, 0.22328, 61.961, 0.92004, 0.82806, 0.22456, 62.353, 0.92666, 0.82025, 0.2257, 62.745, 0.93301, 0.81236, 0.22667, 63.137, 0.93909, 0.80439, 0.22744, 63.529, 0.94489, 0.79634, 0.228, 63.922000000000004, 0.95039, 0.78823, 0.22831, 64.31400000000001, 0.9556, 0.78005, 0.22836, 64.706, 0.96049, 0.77181, 0.22811, 65.098, 0.96507, 0.76352, 0.22754, 65.49000000000001, 0.96931, 0.75519, 0.22663, 65.88199999999999, 0.97323, 0.74682, 0.22536, 66.27499999999999, 0.97679, 0.73842, 0.22369, 66.66699999999999, 0.98, 0.73, 0.22161, 67.059, 0.98289, 0.7214, 0.21918, 67.45100000000001, 0.98549, 0.7125, 0.2165, 67.843, 0.98781, 0.7033, 0.21358, 68.235, 0.98986, 0.69382, 0.21043, 68.62700000000001, 0.99163, 0.68408, 0.20706, 69.02000000000001, 0.99314, 0.67408, 0.20348, 69.41199999999999, 0.99438, 0.66386, 0.19971, 69.804, 0.99535, 0.65341, 0.19577, 70.196, 0.99607, 0.64277, 0.19165, 70.588, 0.99654, 0.63193, 0.18738, 70.98, 0.99675, 0.62093, 0.18297, 71.37299999999999, 0.99672, 0.60977, 0.17842, 71.765, 0.99644, 0.59846, 0.17376, 72.15700000000001, 0.99593, 0.58703, 0.16899, 72.54899999999999, 0.99517, 0.57549, 0.16412, 72.941, 0.99419, 0.56386, 0.15918, 73.333, 0.99297, 0.55214, 0.15417, 73.725, 0.99153, 0.54036, 0.1491, 74.118, 0.98987, 0.52854, 0.14398, 74.51000000000002, 0.98799, 0.51667, 0.13883, 74.902, 0.9859, 0.50479, 0.13367, 75.29400000000001, 0.9836, 0.49291, 0.12849, 75.68599999999999, 0.98108, 0.48104, 0.12332, 76.07800000000002, 0.97837, 0.4692, 0.11817, 76.471, 0.97545, 0.4574, 0.11305, 76.863, 0.97234, 0.44565, 0.10797, 77.255, 0.96904, 0.43399, 0.10294, 77.647, 0.96555, 0.42241, 0.09798, 78.039, 0.96187, 0.41093, 0.0931, 78.431, 0.95801, 0.39958, 0.08831, 78.82400000000001, 0.95398, 0.38836, 0.08362, 79.216, 0.94977, 0.37729, 0.07905, 79.608, 0.94538, 0.36638, 0.07461, 80.0, 0.94084, 0.35566, 0.07031, 80.392, 0.93612, 0.34513, 0.06616, 80.784, 0.93125, 0.33482, 0.06218, 81.176, 0.92623, 0.32473, 0.05837, 81.569, 0.92105, 0.31489, 0.05475, 81.961, 0.91572, 0.3053, 0.05134, 82.353, 0.91024, 0.29599, 0.04814, 82.745, 0.90463, 0.28696, 0.04516, 83.137, 0.89888, 0.27824, 0.04243, 83.529, 0.89298, 0.26981, 0.03993, 83.922, 0.88691, 0.26152, 0.03753, 84.314, 0.88066, 0.25334, 0.03521, 84.70600000000002, 0.87422, 0.24526, 0.03297, 85.098, 0.8676, 0.2373, 0.03082, 85.49, 0.86079, 0.22945, 0.02875, 85.882, 0.8538, 0.2217, 0.02677, 86.275, 0.84662, 0.21407, 0.02487, 86.667, 0.83926, 0.20654, 0.02305, 87.059, 0.83172, 0.19912, 0.02131, 87.45100000000001, 0.82399, 0.19182, 0.01966, 87.843, 0.81608, 0.18462, 0.01809, 88.235, 0.80799, 0.17753, 0.0166, 88.627, 0.79971, 0.17055, 0.0152, 89.02, 0.79125, 0.16368, 0.01387, 89.412, 0.7826, 0.15693, 0.01264, 89.804, 0.77377, 0.15028, 0.01148, 90.196, 0.76476, 0.14374, 0.01041, 90.58800000000001, 0.75556, 0.13731, 0.00942, 90.98, 0.74617, 0.13098, 0.00851, 91.373, 0.73661, 0.12477, 0.00769, 91.765, 0.72686, 0.11867, 0.00695, 92.157, 0.71692, 0.11268, 0.00629, 92.549, 0.7068, 0.1068, 0.00571, 92.941, 0.6965, 0.10102, 0.00522, 93.333, 0.68602, 0.09536, 0.00481, 93.72500000000001, 0.67535, 0.0898, 0.00449, 94.118, 0.66449, 0.08436, 0.00424, 94.51, 0.65345, 0.07902, 0.00408, 94.90200000000002, 0.64223, 0.0738, 0.00401, 95.294, 0.63082, 0.06868, 0.00401, 95.686, 0.61923, 0.06367, 0.0041, 96.078, 0.60746, 0.05878, 0.00427, 96.47099999999999, 0.5955, 0.05399, 0.00453, 96.863, 0.58336, 0.04931, 0.00486, 97.255, 0.57103, 0.04474, 0.00529, 97.64699999999999, 0.55852, 0.04028, 0.00579, 98.039, 0.54583, 0.03593, 0.00638, 98.431, 0.53295, 0.03169, 0.00705, 98.824, 0.51989, 0.02756, 0.0078, 99.216, 0.50664, 0.02354, 0.00863, 99.60799999999999, 0.49321, 0.01963, 0.00955, 100.0, 0.4796, 0.01583, 0.01055]
# fmt: on
deviationLUT.ColorSpace = "RGB"
deviationLUT.ScalarRangeInitialized = 1.0

# create a new 'Threshold' and scale it
threshold1 = Threshold(registrationName="Threshold1", Input=file_deviation_vtk)
threshold1.Scalars = ["POINTS", "deviation"]
threshold1.UpperThreshold = 100.0
Hide(file_deviation_vtk, renderView1)
threshold1.UpperThreshold = 99.0
renderView1.Update()
deviationLUT.ApplyPreset("Turbo", True)
deviationPWF.RescaleTransferFunction(0, 1)
renderView1.Update()
UpdatePipeline(time=0.0, proxy=threshold1)

# Do an 'Extract Surface' from the threshold object
extractSurface1 = ExtractSurface(registrationName="ExtractSurface1", Input=threshold1)
extractSurface1.PieceInvariant = 1
extractSurface1.NonlinearSubdivisionLevel = 1
extractSurface1.FastMode = 0
try:
    extractSurface1.RemoveGhostInterfaces = 1
except:
    pass
UpdatePipeline(time=0.0, proxy=extractSurface1)

# Do a 'Generate Surface Normals' from the Extraced surfaces object
generateSurfaceNormals1 = GenerateSurfaceNormals(registrationName="GenerateSurfaceNormals1", Input=extractSurface1)
generateSurfaceNormals1.FeatureAngle = 30.0
generateSurfaceNormals1.Splitting = 1
generateSurfaceNormals1.Consistency = 1
generateSurfaceNormals1.FlipNormals = 0
generateSurfaceNormals1.NonManifoldTraversal = 1
generateSurfaceNormals1.ComputeCellNormals = 0
generateSurfaceNormals1.PieceInvariant = 1
UpdatePipeline(time=0.0, proxy=generateSurfaceNormals1)
renderView1.Update()
generateSurfaceNormals1Display = Show(generateSurfaceNormals1, renderView1, "UnstructuredGridRepresentation")
generateSurfaceNormals1Display.SetScalarBarVisibility(renderView1, True)
generateSurfaceNormals1Display.RescaleTransferFunctionToDataRange(False, True)

# save the Deviation in a .Ply file
SaveData(
    path_outputfile_num,
    proxy=generateSurfaceNormals1,
    ChooseArraysToWrite=0,
    PointDataArrays=["Normals", "deviation"],
    CellDataArrays=[],
    FieldDataArrays=[],
    VertexDataArrays=[],
    EdgeDataArrays=[],
    RowDataArrays=[],
    WriteTimeSteps=0,
    Filenamesuffix="_%d",
    NumberOfIORanks=1,
    RankAssignmentMode="Contiguous",
    FileType="Binary",
    EnableColoring=1,
    EnableAlpha=0,
    ColorArrayName=["POINTS", "deviation"],
    LookupTable=deviationLUT,
)

# =============================================================================================================
# 1) E X P E R I M E N T A L   C R A C K   S U R F A C E
# =============================================================================================================

# Open the experimental crack file : create a new 'XML Unstructured Grid Reader'
file_experimental_vtk = LegacyVTKReader(registrationName=path_inputfile_exp, FileNames=[path_inputfile_exp])

# show data in view
file_experimentalvtkDisplay = Show(file_experimental_vtk, renderView1, "StructuredGridRepresentation")

# turn off scalar coloring
ColorBy(file_experimentalvtkDisplay, None)

# update the view to ensure updated data information
renderView1.Update()
UpdatePipeline(time=0.0, proxy=file_experimental_vtk)

# Do an "Extract Surface"
extractSurface2 = ExtractSurface(registrationName="ExtractSurface2", Input=file_experimental_vtk)
extractSurface2.PieceInvariant = 1
extractSurface2.NonlinearSubdivisionLevel = 1
extractSurface2.FastMode = 0
try:
    extractSurface1.RemoveGhostInterfaces = 1
except:
    pass
UpdatePipeline(time=0.0, proxy=extractSurface2)

# Do a 'Generate Surface Normals'
generateSurfaceNormals2 = GenerateSurfaceNormals(registrationName="GenerateSurfaceNormals1", Input=extractSurface2)
generateSurfaceNormals2.FeatureAngle = 30.0
generateSurfaceNormals2.Splitting = 1
generateSurfaceNormals2.Consistency = 1
generateSurfaceNormals2.FlipNormals = 0
generateSurfaceNormals2.NonManifoldTraversal = 1
generateSurfaceNormals2.ComputeCellNormals = 0
generateSurfaceNormals2.PieceInvariant = 1
generateSurfaceNormals1Display = GetDisplayProperties(generateSurfaceNormals2, view=renderView1)
ColorBy(generateSurfaceNormals1Display, None)
UpdatePipeline(time=0.0, proxy=generateSurfaceNormals1Display)
renderView1.Update()

# save the Experimental surface in a .ply file
SaveData(
    path_outputfile_exp,
    proxy=generateSurfaceNormals2,
    ChooseArraysToWrite=0,
    PointDataArrays=["Normals"],
    CellDataArrays=[],
    FieldDataArrays=[],
    VertexDataArrays=[],
    EdgeDataArrays=[],
    RowDataArrays=[],
    WriteTimeSteps=0,
    Filenamesuffix="_%d",
    NumberOfIORanks=1,
    RankAssignmentMode="Contiguous",
    FileType="Binary",
    EnableColoring=0,
    EnableAlpha=0,
    ColorArrayName=["POINTS", ""],
    LookupTable=None,
)


# print success message
print("successfully output the file ", path_outputfile_num, "!")
print("successfully output the file ", path_outputfile_exp, "!")

# ------------------------------------- READ ME ------------------------------------------------------
# >>>>>>>>>>>>>>>
# The following python script is used in Blender python terminal and serves as an initiation template
# The script below creates an animated vizualization of the (2D) Cahn Hilliard dynamics in .usdc file.
# The script does the following:
# - Imports different .ply objects
# - Applies different transformations: scaling, translations, ...
# - Creates a .usdc file which then later can be uploaded on a webpage and vizualized with AR on iOs
# Locally, the .usdc file can be vizualized respectively in https://www.usdz-viewer.net
# >>>>>>>>>>>>>>>
# To check on the final results, please visit https://ar.compmech.ethz.ch
# >>>>>>>>>>>>>>>
# This script is partly based on the work of Dr. Mrudang Mathur of Soft Tissue Biomechanics Lab - UT Austin
# to cite his work :
# https://doi.org/10.1016/j.finel.2022.103851
# "A brief note on building augmented reality models for scientific visualization"
# -----------------------------------------------------------------------------------------------------

# ****************************************************************************************************************************
# Approach: To export an .usdc animation, the .ply files should have the same number of faces and vertexes. Besides, the animation process
# is based on defining key shapes, where each object representes a deformation frame.
# ****************************************************************************************************************************
# Import necessary Blender libraries
import bpy
from mathutils import *
from math import *

# Remove any initial object / collection in the Layout
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()

# ======= Definition paths and objects names & other necessary inputs ===========
pathName = input("enter the absolute path for importing the cahn hilliard .ply file")  # Path to import the concentrations ply files
fileName = "ply_file"  # Base name of the ply files
startFrame = 1  # File no. of first object in timeseries
endFrame = 200  # File no. of last object in timeseries
numRange = range(endFrame, startFrame - 1, -1)

isoScale = 1  # Isotropic scaling factor for object size

key_index = 1  # Shape key (morphlogy/mesh) ID in timeseries
init_frame = 1  # Frame no. of first frame in animation
step = 2  # No. of empty frames between two meshes in animation; like "frame-rate"
nloops = 2  # No. of loops in animation, necessay for USDZ to avoid steady still animations
counter = init_frame  # Frames counter

# ======= Import objects to the scene and apply necessary transformations ===========
for i in numRange:

    # IMPORT OBJECTS
    tempfileName = pathName + fileName + "_" + str(i) + ".ply"
    print(tempfileName)
    bpy.ops.import_mesh.ply(filepath=tempfileName)

    # TRANSFORM OBJECTS
    bpy.context.object.scale[0] = isoScale
    bpy.context.object.scale[1] = isoScale
    bpy.context.object.scale[2] = isoScale
    bpy.context.object.location[0] = -0.5
    bpy.context.object.location[1] = -0.5
    bpy.context.object.location[2] = 1
    bpy.ops.object.shade_smooth()

    # ADD COLOUR MAP
    temp_matName = "Material_" + str(i)
    new_mat = bpy.data.materials.new(name=temp_matName)
    bpy.context.object.data.materials.append(new_mat)
    new_mat.use_nodes = True
    nodes = new_mat.node_tree.nodes
    material_output = nodes.get("Material Output")
    material_input = nodes.get("Material Input")
    node_attribute = nodes.new(type="ShaderNodeAttribute")
    node_attribute.attribute_name = "Col"
    new_mat.node_tree.links.new(
        node_attribute.outputs[0], bpy.data.materials[temp_matName].node_tree.nodes["Principled BSDF"].inputs[0]
    )

    # # Decimate geometry to reduce the final filesize
    # bpy.ops.object.modifier_add(type='DECIMATE')
    # bpy.context.object.modifiers["Decimate"].ratio = 0.1

# ======= Animate objects ===========
for obj in bpy.data.collections["Collection"].all_objects:  # Select all objects
    obj.select_set(True)
bpy.ops.object.join_shapes()  # Transform each object into a deformed state (shape key) relative to the first object

# Create the animation frames
for j in range(0, nloops):

    for i in range(startFrame + 1, endFrame + 1):

        tempName = fileName + "_" + str(i)
        bpy.context.object.active_shape_key_index = key_index  # Retriev the current deformed state (shape key)
        bpy.data.shape_keys["Key"].key_blocks[
            tempName
        ].value = 0  # The deformed state is visible in the previous frames
        bpy.data.shape_keys["Key"].key_blocks[tempName].keyframe_insert("value", frame=counter)
        bpy.data.shape_keys["Key"].key_blocks[tempName].value = 1  # The deformed state is visible in the current frame
        bpy.data.shape_keys["Key"].key_blocks[tempName].keyframe_insert("value", frame=counter + step)
        bpy.data.shape_keys["Key"].key_blocks[
            tempName
        ].value = 0  # The deformed state is not visible from the next frames
        bpy.data.shape_keys["Key"].key_blocks[tempName].keyframe_insert("value", frame=counter + 2 * step)
        counter = counter + step  # Update frame counter
        key_index = key_index + 1  # Update key for the next one
# Set the final frame
bpy.context.scene.frame_end = init_frame + (endFrame - startFrame) * step * nloops + step
# Remove all the objects from the scene except the first one and its associated shape keys to make the exporting process lighter
for obj in bpy.data.collections["Collection"].all_objects:
    obj.select_set(False)
# delete extra frames if any
for i in range(endFrame, startFrame, -1):
    bpy.data.objects[fileName + "_" + str(i)].select_set(True)
    bpy.ops.object.delete()

# Export the .usdc animation
bpy.data.collections["Collection"].objects[fileName + "_" + str(startFrame)].select_set(True)
bpy.ops.wm.usd_export(filepath=pathName + fileName + "_dynamic.usdc", export_animation=True, selected_objects_only=True)

# ------------------------------------- READ ME ------------------------------------------------------
# >>>>>>>>>>>>>>>
# The following python script is used in Blender python terminal and serves as an initiation template
# The script below creates an animated file to visualize of the (2D) Cahn Hilliard dynamics in .glb file
# The script does the following:
# - Imports different .ply objects
# - Applies different transformations: scaling, translations, ...
# - Creates a .glb file which then later can be uploaded on a webpage and vizualized with AR on Android
# Locally, the .glb file can be vizualized respectively in https://sandbox.babylonjs.com/
# >>>>>>>>>>>>>>>
# To check on the final results, please visit https://ar.compmech.ethz.ch
# >>>>>>>>>>>>>>>
# This script is partly based on the work of Dr. Mrudang Mathur of Soft Tissue Biomechanics Lab - UT Austin
# to cite his work :
# https://doi.org/10.1016/j.finel.2022.103851
# "A brief note on building augmented reality models for scientific visualization"
# -----------------------------------------------------------------------------------------------------

# ****************************************************************************************************************************
# Approach: To export an .glb animation, we consecutively scale up and down the objects
# ****************************************************************************************************************************
# Import necessary Blender libraries
import bpy
from mathutils import *
from math import *

# Remove any initial object / collection in the Layout
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()

# ======= Definition paths and objects names & other necessary inputs ===========
pathName = input("enter the absolute path for importing the cahn hilliard .ply file")  # Path to import the concentrations ply files
fileName = "ply_file"  # Base name of the ply files
startFrame = 1  # File no. of first object in timeseries
stepFrame = 1  # Step to skip files
endFrame = 200  # File no. of last object in timeseries
numRange = range(startFrame, endFrame + 1, stepFrame)

transform_scale = 1.0  # Scaling factor
Decimate_ratio = 0.1  # Decimation factor to coarsen the mesh
isoScale = 1.0  # Isotropic scaling factor for object size during animation
init_frame = 1  # Frame no. of first frame in animation
animStep = 2  # like "frame-rate"
count = 1  # Frame counters

# ======= Import objects to the scene and apply necessary transformations ===========
for i in numRange:

    # IMPORT OBJECTS
    tempfileName = pathName + fileName + "_" + str(i) + ".ply"
    print(tempfileName)
    bpy.ops.import_mesh.ply(filepath=tempfileName)

    # TRANSFORM OBJECTS
    bpy.context.object.scale[0] = transform_scale
    bpy.context.object.scale[1] = transform_scale
    bpy.context.object.scale[2] = transform_scale
    bpy.context.object.location[0] = -0.5
    bpy.context.object.location[1] = -0.5
    bpy.context.object.location[2] = 1
    bpy.ops.object.shade_smooth()  # smooth the surface

    # Add Material and call the colors from .ply file
    temp_matName = "Material_" + str(i)
    new_mat = bpy.data.materials.new(name=temp_matName)
    bpy.context.object.data.materials.append(new_mat)
    new_mat.use_nodes = True
    nodes = new_mat.node_tree.nodes
    material_output = nodes.get("Material Output")
    material_input = nodes.get("Material Input")
    node_attribute = nodes.new(type="ShaderNodeAttribute")
    node_attribute.attribute_name = "Col"
    new_mat.node_tree.links.new(
        node_attribute.outputs[0], bpy.data.materials[temp_matName].node_tree.nodes["Principled BSDF"].inputs[0]
    )

    # Decimate geometry to reduce the final filesize
    bpy.ops.object.modifier_add(type="DECIMATE")
    bpy.context.object.modifiers["Decimate"].ratio = Decimate_ratio


# ======= Animate objects ===========

# Create the animation frames
for i in range(startFrame, endFrame, stepFrame):

    bpy.data.objects[fileName + "_" + str(i)].scale = [isoScale, isoScale, isoScale]  # Current object scaled up
    bpy.data.objects[fileName + "_" + str(i)].keyframe_insert(
        data_path="scale", frame=count
    )  # Save the scaled up current object in the current frame
    bpy.data.objects[fileName + "_" + str(i + 1)].scale = [0.0, 0.0, 0.0]  # Next object scaled down
    bpy.data.objects[fileName + "_" + str(i + 1)].keyframe_insert(
        data_path="scale", frame=count
    )  # Save the scaled down next object in the current frame

    bpy.data.objects[fileName + "_" + str(i)].scale = [0.0, 0.0, 0.0]  # Current object scaled down
    bpy.data.objects[fileName + "_" + str(i)].keyframe_insert(
        data_path="scale", frame=count + animStep
    )  # Save the scaled down current object in the next frame
    bpy.data.objects[fileName + "_" + str(i + 1)].scale = [isoScale, isoScale, isoScale]  # Next object scaled up
    bpy.data.objects[fileName + "_" + str(i + 1)].keyframe_insert(
        data_path="scale", frame=count + animStep
    )  # Save the scaled up next object in the next frame

    count = count + animStep  # Update frame counter

# Adjust interpolation curves
for obj in bpy.data.objects:
    fcurves = obj.animation_data.action.fcurves
    for fcurve in fcurves:
        for kf in fcurve.keyframe_points:
            kf.interpolation = "CONSTANT"

# adjust frame range of the animation
bpy.context.scene.frame_end = init_frame + (endFrame - startFrame + 1) * animStep

# ADD LIGHTING (Optional)
# light_data = bpy.data.lights.new(name="my-light-data", type='POINT')
# light_data.energy = 100
# light_object = bpy.data.objects.new(name="LIGHT", object_data=light_data)
# bpy.context.collection.objects.link(light_object)
# light_object.location = (0, 0, 0.5)
# bpy.data.collections['Collection'].objects['LIGHT'].select_set(True)

# Export the .glb animation
bpy.ops.export_scene.gltf(filepath=pathName + "/" + fileName + "_dynamic.glb", export_nla_strips=False)

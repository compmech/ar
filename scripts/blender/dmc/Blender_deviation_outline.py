# ------------------------------------- READ ME ------------------------------------------------------
# >>>>>>>>>>>>>>>
# The following python script is used in Blender python terminal and serves as an initiation template
# The script below creates the static AR objects for the Damage Mechanics Challenge.
# The script does the following:
# - Imports different .ply objects 
# - Applies different transformations: scaling, translations, add light...
# - Creates a .glb and .usdz files which then later can be uploaded on a webpage and vizualize them with AR on Android or Iphone
# Locally, the .glb and .usdz files can be vizualized respectively in https://sandbox.babylonjs.com/ and https://products.aspose.app/3d/fr/viewer/usdz 
# >>>>>>>>>>>>>>>
# To check on the final results, please visit https://ar.compmech.ethz.ch
# >>>>>>>>>>>>>>>
# This script is partly based on the work of Dr. Mrudang Mathur of Soft Tissue Biomechanics Lab - UT Austin 
# to cite his work : 
# https://doi.org/10.1016/j.finel.2022.103851
# "A brief note on building augmented reality models for scientific visualization"
# -----------------------------------------------------------------------------------------------------

# ****************************************************************************************************************************
# ====== We import the experimental crack surface, the deviation with respect to numerics, and the geometry outline ==========
# **************************************************************************************************************************** 

### Import necessary Blender packages #
import bpy
from bpy import data as D
from bpy import context as C
from mathutils import *
from math import *
import importlib
import io_scene_usdz
importlib.reload(io_scene_usdz)
import io_scene_usdz.export_usdz
from io_scene_usdz.export_usdz import export_usdz
###

# Clean any initial object / collection in the Layout
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()
bpy.ops.outliner.orphans_purge()

# ======= Definition paths and objects names & other necessary inputs ===========

# Define parent folder path
pathfold_crack      = input("enter the absolute path for importing the experimental crack surface .ply file") # Path to import experimental crack
pathfold_deviation  = input("enter the absolute path for importing the deviation surface .ply file") # Path to import the deviation surface
pathName_save_glb   = input("enter the absolute path for exporting AR .glb file")  # Path where to export the .glb file
pathName_save_usdz  = input("enter the absolute path for exporting AR .usdz file") # Path where to export the .usdz file

# Names of the experimental cracks .ply
idx_file_access        = 1 #  TO CHANGE  <==== Which file to access: 0-> HCH, 1->HA, 2->HB, 3->HC, 4->H45
files_ID_string        = ['HCH', 'HA', 'HB', 'HC', 'H45'] 
ID_file_string         = files_ID_string[idx_file_access]
Extension_crack_file   = '.ply'
name_crack_file        = ID_file_string + '_Experimental_Crack_0'
path_crack_file        = pathfold_crack + name_crack_file + Extension_crack_file

# Name of the Deviation .ply
Extension_deviation_file  = '.ply'
name_deviation_file       = ID_file_string + '_Deviation_Crack_0'
path_deviation_file       = pathfold_deviation + name_deviation_file + Extension_deviation_file

# Name of the ar files
name_save_model     = ID_file_string + '_Deviation_AR_model'

# Transformations to apply on experimental crack
scale_crack       = 0.1  # Scaling in the 3 directions
scale_crack_y     = 0.1  # Additional scaling to apply only in y direction
translate_crack_z = 2.75 # Translation in z direction

# Transformations to apply on deviation crack .ply
scale_deviation       = 0.1  # Scaling in the 3 directions
scale_deviation_y     = 0.1  # Scaling to apply only in y direction
translate_deviation_z = 2.75 # Translation in z direction
Deviation_matName     = "Material_0" # Define a "material" name for the Deviation to retrieve the colors
Experimental_matName  = "Material_exp" # Define a "material" name for the experimental crack

# Initializing the outlines
def create_cylinder_between_points(point1, point2, radius, id_cylinder, scaling, z_position):
    # Calculate the center of the cylinder's top and basis
    center = scaling * (point1 + point2) / 2
    height = scaling * (point2 - point1).length
    
    # Name of the cylinder
    name_cylinder = "ColorMaterial"+"_cyl_"+str(id_cylinder)
    # Create the cylinder
    bpy.ops.mesh.primitive_cylinder_add(radius=radius, depth=height, location=center)
    cylinder      = bpy.context.object
    cylinder.name = name_cylinder
    # Calculate the direction vector
    direction = point2 - point1
    # Calculate the rotation needed to align the cylinder with the direction vector
    rotation_matrix = direction.to_track_quat('Z', 'Y').to_matrix().to_4x4()
    # Set the rotation of the cylinder
    cylinder.matrix_world = Matrix.Translation(center) @ rotation_matrix
    # Set the 'z' position of the Crack (above surface)
    bpy.data.objects[name_cylinder].location[2]  =bpy.data.objects[name_cylinder].location[2] + z_position
    # Add material property
    Object_select = bpy.data.objects[name_cylinder] # Select the crack object
    bpy.ops.object.select_all(action='DESELECT')
    Object_select.select_set(True)
    bpy.context.view_layer.objects.active = Object_select
    # Create a new material property related to the experimental crack object
    new_mat = bpy.data.materials.new(name=name_cylinder)
    bpy.context.object.data.materials.append(new_mat)
    new_mat.use_nodes = True
    return cylinder
id_cylinder         = 1
scale_outline       = 0.1  # Scaling in the 3 directions
translate_outline_z = 2.75 # Translation in z direction
radius_outline      = scale_outline*0.1  # Radius out the outlines 

# ======= Import objects to the scene and apply necessary transformations ===========

# Import Objects Experimental crack and the deviation --------------------------
bpy.ops.import_mesh.ply(filepath=path_crack_file)     # Import experimental crack object
bpy.ops.import_mesh.ply(filepath=path_deviation_file) # Import numerical deviation object
#  TRANSFORM OBJECTS 
for i in range(0,3):
    # Scale the crack in its three directions
    bpy.data.objects[name_crack_file].scale[i] = scale_crack
    # Scale the deviation in its three directions
    bpy.data.objects[name_deviation_file].scale[i] = scale_crack
# Set the 'z' position of the Crack (above surface)
bpy.data.objects[name_crack_file].location[2] = translate_crack_z
# Set the 'z' position of the Deviation (above surface)
bpy.data.objects[name_deviation_file].location[2] = translate_deviation_z
# Give a mirror to the crack 
bpy.data.objects[name_crack_file].scale[1]     = scale_crack_y
bpy.data.objects[name_deviation_file].scale[1] = scale_deviation_y

# Add colour map to the crack deviation
Object_select  = bpy.data.objects[name_deviation_file] # Select the crack object
bpy.context.view_layer.objects.active = Object_select
# Create a new material property related to the crack deviation object
new_mat           = bpy.data.materials.new(name=Deviation_matName)
bpy.context.object.data.materials.append(new_mat)
new_mat.use_nodes = True
# Get the colors from the ply file and relate them to the crack object
nodes             = new_mat.node_tree.nodes
material_output   = nodes.get("Material Output")
material_input    = nodes.get("Material Input")
node_attribute = nodes.new(type="ShaderNodeAttribute")
node_attribute.attribute_name = "Col"
new_mat.node_tree.links.new(node_attribute.outputs[0], bpy.data.materials[Deviation_matName].node_tree.nodes["Principled BSDF"].inputs[0])

# Add material property to the experimental crack
Object_select = bpy.data.objects[name_crack_file] # Select the crack object
bpy.ops.object.select_all(action='DESELECT')
Object_select.select_set(True)
bpy.context.view_layer.objects.active = Object_select
# Create a new material property related to the experimental crack object
new_mat_2 = bpy.data.materials.new(name=Experimental_matName)
bpy.context.object.data.materials.append(new_mat_2)
new_mat_2.use_nodes = True

# Add light above the objects
light_data = bpy.data.lights.new(name="my-light-data", type='SUN')
light_data.energy = 2.0
light_object = bpy.data.objects.new(name="LIGHT", object_data=light_data)
bpy.context.collection.objects.link(light_object)
light_object.location = (0, 0, 3.0)
bpy.data.collections['Collection'].objects['LIGHT'].select_set(True)

# Create the outlines --------------------------
# Top face coordinates arrays
top_face_pts_1  = [Vector((-38.1, -6.35, 0)), Vector((-38.1, 6.35, 0)), Vector((-38.1, 6.35, 0)), Vector((38.1, 6.35, 0))]
top_face_pts_2  = [Vector((38.1, -6.35, 0)), Vector((38.1, 6.35, 0)), Vector((-38.1, -6.35, 0)), Vector((38.1, -6.35, 0))]
nb_pts_top_face = range(0,4)
for i in nb_pts_top_face:
    pt1 = top_face_pts_1[i]
    pt2 = top_face_pts_2[i]
    create_cylinder_between_points(pt1, pt2, radius_outline, id_cylinder, scale_outline, translate_outline_z)
    id_cylinder = id_cylinder + 1
# Left face
left_face_pts_1 = [Vector((-38.1, -6.35, 0)), Vector((-38.1, 6.35, 0)), Vector((-38.1, 6.35, -25.4))]
left_face_pts_2 = [Vector((-38.1, -6.35, -25.4)), Vector((-38.1, 6.35, -25.4)), Vector((-38.1, -6.35, -25.4))]
nb_pts_left_face = range(0,3)
for i in nb_pts_left_face:
    pt1 = left_face_pts_1[i]
    pt2 = left_face_pts_2[i]
    create_cylinder_between_points(pt1, pt2, radius_outline, id_cylinder, scale_outline, translate_outline_z)
    id_cylinder = id_cylinder + 1
# Right face
right_face_pts_1 = [Vector((38.1, -6.35, 0)), Vector((38.1, 6.35, 0)), Vector((38.1, 6.35, -25.4))]
right_face_pts_2 = [Vector((38.1, -6.35, -25.4)), Vector((38.1, 6.35, -25.4)), Vector((38.1, -6.35, -25.4))]
nb_pts_right_face = range(0,3)
for i in nb_pts_right_face:
    pt1 = right_face_pts_1[i]
    pt2 = right_face_pts_2[i]
    create_cylinder_between_points(pt1, pt2, radius_outline, id_cylinder, scale_outline, translate_outline_z)
    id_cylinder = id_cylinder + 1
# = Define the coordinates to create the crack outline
    # ['HCH', 'HA', 'HB', 'HC', 'H45'] 
if idx_file_access == 4:    #H45
    N1 = [-5.64, -6.35, -25.4]
    N2 = [7.06, 6.35, -25.4]
    N3 = [-7.06, -6.35, -25.4]
    N4 = [5.64, 6.35, -25.4]
    N5 = [-5.64, -6.35, -20.32]
    N6 = [7.06, 6.35, -20.32]
    N7 = [-7.06, -6.35, -20.32]
    N8 = [5.64, 6.35, -20.32]
elif idx_file_access == 1:   #HA
    N1 = [19.55, -6.35, -25.4]
    N2 = [19.55, 6.35, -25.4]
    N3 = [18.55, -6.35, -25.4]
    N4 = [18.55, 6.35, -25.4]
    N5 = [19.55, -6.35, -20.32]
    N6 = [19.55, 6.35, -20.32]
    N7 = [18.55, -6.35, -20.32]
    N8 = [18.55, 6.35, -20.32]
elif idx_file_access == 2:    #HB
    N1 = [10.02, -6.35, -25.4]
    N2 = [10.02, 6.35, -25.4]
    N3 = [9.02, -6.35, -25.4]
    N4 = [9.02, 6.35, -25.4]
    N5 = [10.02, -6.35, -20.32]
    N6 = [10.02, 6.35, -20.32]
    N7 = [9.02, -6.35, -20.32]
    N8 = [9.02, 6.35, -20.32]
elif idx_file_access == 3:   #HC
    N1 = [0.5, -6.35, -25.4]
    N2 = [0.5, 6.35, -25.4]
    N3 = [-0.5, -6.35, -25.4]
    N4 = [-0.5, 6.35, -25.4]
    N5 = [0.5, -6.35, -20.32]
    N6 = [0.5, 6.35, -20.32]
    N7 = [-0.5, -6.35, -20.32]
    N8 = [-0.5, 6.35, -20.32]
elif idx_file_access == 0:   #HCH
    N1 = [-12.14, -6.35 -25.4]
    N2 = [-5.79, 6.35, -25.4]
    N3 = [-13.26, -6.35, -25.4]
    N4 = [-6.91, 6.35, -25.4]
    N5 = [-12.14, -6.35, -20.32]
    N6 = [-5.79, 6.35, -22.86]
    N7 = [-13.26, -6.35, -20.32]
    N8 = [-6.91, 6.35, -22.86]
# Lower faces 
lower_face_pts_1 = [Vector((38.1, -6.35, -25.4)), Vector((38.1, 6.35, -25.4)), Vector((N1[0], N1[1], N2[2])), Vector((-38.1, -6.35, -25.4)), Vector((-38.1, 6.35, -25.4)),  Vector((N3[0], N3[1], N3[2]))]
lower_face_pts_2 = [Vector((N1[0], N1[1], N1[2])), Vector((N2[0], N2[1], N2[2])), Vector((N2[0], N2[1], N2[2])), Vector((N3[0], N3[1], N3[2])), Vector((N4[0], N4[1], N4[2])),  Vector((N4[0], N4[1], N4[2]))]
nb_pts_lower_face = range(0,6)
for i in nb_pts_lower_face:
    pt1 = lower_face_pts_1[i]
    pt2 = lower_face_pts_2[i]
    create_cylinder_between_points(pt1, pt2, radius_outline, id_cylinder, scale_outline, translate_outline_z)
    id_cylinder = id_cylinder + 1
# Notch 
notch_pts_1 = [Vector((N5[0], N5[1], N5[2])), Vector((N7[0], N7[1], N7[2])), Vector((N5[0], N5[1], N5[2])), Vector((N8[0], N8[1], N8[2])), Vector((N5[0], N5[1], N5[2])), Vector((N6[0], N6[1], N6[2])),  Vector((N7[0], N7[1], N7[2])), Vector((N8[0], N8[1], N8[2]))]                  
notch_pts_2 = [Vector((N6[0], N6[1], N6[2])), Vector((N8[0], N8[1], N8[2])), Vector((N7[0], N7[1], N7[2])), Vector((N6[0], N6[1], N6[2])), Vector((N1[0], N1[1], N1[2])), Vector((N2[0], N2[1], N2[2])),  Vector((N3[0], N3[1], N3[2])), Vector((N4[0], N4[1], N4[2]))]
nb_pts_notch_face = range(0,8)
for i in nb_pts_notch_face:
    pt1 = notch_pts_1[i]
    pt2 = notch_pts_2[i]
    create_cylinder_between_points(pt1, pt2, radius_outline, id_cylinder, scale_outline, translate_outline_z)
    id_cylinder = id_cylinder + 1
bpy.ops.object.select_all(action='SELECT')
# ============================================ EXPORT FILES ===============================================
# EXPORT FILE with .glb (Android) extension
bpy.ops.export_scene.gltf(filepath = pathName_save_glb + name_save_model + "_glb.glb")
# ---------------------------------------------------------------------------------------------------------

################################################################################################################
# =========== Solve issue of one sided surface colours of IOS model ============================================
# This is mainly due to the usdz exporter of iOs, meaning that only one side of a surface can be shown in AR
# The issue is not present on Android 
################################################################################################################

# ==========> Treat the Deviation surface
# We duplicate the colored deviation surface and change the normal surfaces orientation
# Select the crack deviation object:
Object_select  = bpy.data.objects[name_deviation_file] # Select the crack object
bpy.ops.object.select_all(action='DESELECT')
Object_select.select_set(True)
bpy.context.view_layer.objects.active = Object_select
# Duplicate the object:
bpy.ops.object.duplicate()
duplicate_object = bpy.context.active_object
bpy.context.view_layer.objects.active = duplicate_object
# Flip the normals in the mesh
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.flip_normals()
bpy.ops.object.mode_set(mode='OBJECT')

# ========> Treat the experimental crack surface 
# We assign a default material type to the crack surface to avoid a "pink" surface crack after export
# Select the experimental crack object again to duplicate
Object_select_2  = bpy.data.objects[name_crack_file] # Select the crack object
bpy.ops.object.select_all(action='DESELECT')
Object_select_2.select_set(True)
bpy.context.view_layer.objects.active = Object_select_2
# # Create a new material property related to the experimental crack object
# new_mat_2           = bpy.data.materials.new(name=name_crack_file)
# bpy.context.object.data.materials.append(new_mat_2)
# new_mat_2.use_nodes = True
# Duplicate the object:
bpy.ops.object.duplicate()
duplicate_object_2 = bpy.context.active_object
bpy.context.view_layer.objects.active = duplicate_object_2
# Flip the normals in the mesh
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.flip_normals()
bpy.ops.object.mode_set(mode='OBJECT')

# EXPORT FILE with .usdz (Iphone) extension
bpy.ops.object.select_all(action='SELECT')
export_usdz(context = bpy.context, filepath = pathName_save_usdz + name_save_model + '_usdz.usdz', bakeTextures = True)

